
# [WIP, always*] AYELEN CHAVEZ - README

Last update: 26.04.2021

## What is this?

This is meant to be a document about me. It’s a bit weird, I know.  
But hopefully, this will help you understand me better and help us work together.  
Nonetheless, it's NOT meant as a replacement for actually talking and getting to know each other.  

Hi, there! :wave:

*Disclaimer: this is a living document since people change, grow and, evolve with time. This document should change accordingly but probably not so often (who has the time!?). I'll try to update it from time to time though.

## Me

* I'm an extrovert. I apologize in advance for that.
* I'm fan of honest open conversations and candid feedback, both given and taken.
* I care, especially about people. If you don't like something I did or say, know that I didn't mean to hurt you.
* I'm a lifelong learner. If I ask a lot of (dumb) questions is not because I don't trust you, it's because I'm learning from you.

In the MBTI personality test, I'm the [giver](https://www.personalityperfect.com/enfj-the-giver-personality-type/)

I recently (August 2019) recorded a podcast. It was my first podcast experience but it defines quite well how I work and what I care about.  
Feel free to take a look [here](http://www.projectoxygen.io/619003/1768189-1-ayelen-chavez-engineering-manager-olx-group).  

## My Job

I'm here to create an environment where talented people like yourself can take initiative to build stable, high-quality solutions that will positively impact our users.  
I will try to help you and unblock you. And I will do my best to clarify any tech, company or, business context for you to work in a focused and smooth way.  
I believe everyone has potential. I will make sure you have a personal development plan (PDP) we can work together towards to so you can reach your full potential and, as I like to see it, the best version of yourself.   

## My Goal

I believe the best manager is the one that is not needed. My goal is to have a team that can smoothly perform for a month without my help.

## 1:1s

**TLDR;** Very few things are more important than talking to you if you want to talk to me. If you need to talk, let’s talk.

* 1-1s are very important to me because they are dedicated space for you to talk about anything and everything you want
* They are meetings primarily for you, and only secondarily for me
* It’s not a status meeting unless you want to talk about it
* 1-1s are flexible -- they can shift and change to fit around our shifting schedules
* Don’t save urgent matters for a 1-1. Ping me ASAP, instead!

Feel free to put something in my calendar, don’t feel like you need to ask first.
Is my calendar full? Send me a message and I’ll very likely be able to move something around.

## My Values

Displayed in order of importance, I came with 5 core values that I hope represent my primary way of being.

1. **Connection:** being part of a community and inspiring others by using the power of empathy and communication.  
   Keywords: Empathy - Inspiring - Communication - Acceptance - Teamwork  

2. **Integrity:** being open and transparent to build trust among my peers.  
   Keywords: Transparency - Trust - Openness  

3. **Positivity:** I use my positive energy and vitality to build a great working environment and inspire others. And since I'm not myself if I don't feel healthy or fit, I find this value highly important. 
   Keywords: Energy - Health - Vitality  

4. **Ownership:** I trust my peers to do the right thing. That is why I tend to delegate easily. That way, achievements, and accomplishments come with a strong sense of accountability and ownership.    
   Keywords: Accomplishment - Accountability - Achievement - Delegation - Do the right thing  

5. **Development:** I believe everyone has the potential to learn and grow. I love learning a lot myself and I have a problem-solving mindset, which has the potential of making me unstoppable.  
   Keywords: Clever - Intelligence - Learning - Problem-solving mindset - Smart - Growth - Potential

## My assumptions 

**You’re good at your job.** I trust you know what you are doing, you wouldn’t be here if you weren’t. If it feels like I’m questioning you it’s because I’m: a) Learning from you b) Trying to gather context or c) I'm being your rubber duck.

**I’m not good at your job.** You know best. I’ll work to provide the necessary context and ask questions to help you brainstorm and validate your ideas but I won’t override you.

**You’ll let me know if you can’t do your job.** One of my main responsibilities is ensuring that you’re set up for success. Occasionally things slip through the cracks and I won’t know I’m letting you down.

**You feel safe debating with me.** I find that ideas improve by being examined from all angles. If it sounds like I’m disagreeing I’m most likely just playing devil’s advocate. This does rely on us being able to have a safe debate.

## How can I help you

**Provide context.** Most of my day is spent collecting, filtering, and sharing context/information from across other projects, domains, and product lines. I’ll try to push information to you as much as I can but feel free to ask about anything else.

**Provide an outside(ish) perspective.** I won’t be working on your project every day but will be close enough to have informed thoughts.

**Praise.** Tell me when things go well, share the things which make you proud, and I'll praise and share appropriately.

**Firefight.** A mistake, if shared, becomes a challenge; if hidden, becomes a failure. I hope you feel confident to come to me if something happens. If you don't, we have a bigger problem.

**Other.** Please let me know how else I can help. 

## Feedback 

Because I think **Feedback** is an important topic, I created a session for it.

Disclaimer: I love feedback. Given and received.
If I give you feedback is because I care and I want to see you thrive. That's why if you give me feedback, I will be grateful.
Rest assured I will prepare for our feedback session!

I believe in good and effective feedback:
1. Given as soon as possible.
2. Structured as [the OSCAR model](https://www.samieltamawy.com/how-to-give-an-effective-feedback-oscar-model/) suggests.
3. Have a clear action plan and follow-up.

That's all for now! See you around!